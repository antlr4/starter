import { InputStream, CommonTokenStream } from "antlr4";
import { LangLexer, LangParser } from "./ExprLang/index.js";
import fs from "fs";

// Read the input from file
const input = fs.readFileSync("./ExprLang/examples/sample.expr", "utf8");

// Create a lexer
const chars = new InputStream(input);
const lexer = new LangLexer(chars);

// Create a parser
const tokens = new CommonTokenStream(lexer);
const parser = new LangParser(tokens);
parser.buildParseTrees = true;

// Parse the input
const tree = parser.program();

// Log the resulting tree
console.log(tree.toStringTree(parser.ruleNames));
