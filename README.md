# How to set up project

1. Open a terminal and navigate to the folder you want to place the project in.
2. Run `git clone https://gitlab.com/antlr4/starter.git && cd starter`.
3. Run `npm install` to install the required package (`antlr4@4.9.0`).
4. Run `code .`
5. Install the Extension `ANTLR4 grammar syntax support v2.3.1`
6. Open `ExprLang.g4` once, add a space at the end of a line and press `Cmd/Ctrl+S` to save, which will trigger formatting and generation of Lexer/Parser files in the `./ExprLang/.antlr/` folder.

# How to start developing

1. Run `npm start`

You should see the following:

```
line 2:9 token recognition error at: '+'
line 2:10 extraneous input 'foo' expecting ';'
(program (def f ( x , y ) { (stat a = (expr 3) foo ;) (stat (expr (expr x) and (expr y)) ;) }) <EOF>)
```

If you do not see this, confirm that the extension is installed and restart VSCode.
If all else fails, run `git checkout no-extension`, `npm install` again, and finally, run `npm run save` in the terminal manually every time you make a change to the .g4 file.

2. If you see the above text, you're set! In other words, there's an error in `sample.expr`! Oh no!

## A challenge appears!

Can you find out how to fix `sample.expr`?
(If you want to jump straight to trying transpilation, switch to the `transpiler` branch!)

---

# Workshop tasks

Once you've fixed `sample.expr`, I have a number of challenges you can choose between. Try out any of the examples alone or in groups, or make up your own goal! Start small, but do whatever you want!

## Design Challenge

Imagine the syntax for a new programming language, and write a program that would solve a task using that programming language! The program can be as complex as you have time to write.

### Examples:

1. Write the fibonacci algorithm in your own made-up programming language.
2. Write the styling for a WebApp component, replacing CSS.
3. Write a "dinner menu -> grocery list" algorithm that takes in a meal recipe (list of ingredients), list of ingredients you already have (in the fridge etc), and prints out what you need to buy.

## Syntax Parsing Challenge

Using what you've learned about ANTLR4 in the `ExprLang` example, write a new grammar (`.g4`) file as well as a new, simple program file (replacing `sample.expr`), and attempt to successfully parse it. Once you do, keep adding grammar rules and expand on your program, parsing it as you go!

### Examples:

1. Parse a file that implements the fibonacci algorithm.
2. Attempt to define small parts of another programming language's syntax, and parse it.

## Transpiler Challenge

Using the generated `Listener` and `Visitor` files, implement a custom Listener and Visitor that takes your parsed code, and generates a text string that you write to a new file, comparing it to a "target" file. Check out the `transpiler` branch for a head start.

### Examples:

1. Transpile your fibonacci algorithm to JavaScript (or any other language), and run that file.
2. Make up a new Chess annotation syntax, and transpile it to the standard format ("Queen takes d4" -> "Qxd4").
3. Take your favorite programming language, and make a version where each core keyword (`func`, `var`, `true`/`false`) are written using emojis, and parse them back to their standard keywords.

## Note:

For the "Syntax Parsing" and "Transpiler" challenges, I recommend you make a copy of the `ExprLang` folder, rename the new folder and each reference in it from `ExprLang` to whatever you want your new language to be called, and delete the `.antlr` folder. Then regenerate it, update the imports in `index.js` and attempt to run `npm start`. If it runs as before, you're ready to make changes and begin your challenge!
