grammar ExprLang;

program: stat EOF | def EOF;

stat: ID EQ expr | expr;

def: ID LPAREN (ID (COMMA ID)*)? RPAREN LCURLY stat* (RET expr)? RCURLY;

expr:
    ID
    | INT
    | func
    | NOT expr
    | expr PLUS expr
    | expr MINUS expr
;

func: ID LPAREN expr (COMMA expr)* RPAREN;

PLUS: '+';
MINUS: '-';
NOT: 'not';
EQ: '=';
COMMA: ',';
SEMI: ';';
LPAREN: '(';
RPAREN: ')';
LCURLY: '{';
RCURLY: '}';
RET: 'ret';

INT: [0-9]+;
ID: [a-zA-Z_][a-zA-Z_0-9]*;
WS: [ \t\n\r\f]+ -> channel(HIDDEN);
